package adyen

// BinGateway - allow BIN Lookup
// methods and flows.
type BinGateway struct {
	*Adyen
}

const (
	getCostEstimate = "getCostEstimate"
)

// BinLookup - Perform BinLookup request in Adyen.
//
// Used to get a bin number for a merchant.
func (a *BinGateway) BinLookup(req *BinLookupRequest) (*BinLookupResponse, error) {
	url := a.adyenURL(BinLookupService, getCostEstimate, BinLookupAPIVersion)

	resp, err := a.execute(url, req)
	if err != nil {
		return nil, err
	}

	return resp.binLookup()
}
