package adyen

// BinLookupRequest contains the fields required by the bin lookup
// API's /BinLookup endpoint.  See the following for more
// information:
//
// https://docs.adyen.com/api-explorer/#/BinLookup/v40/post/getCostEstimate
type BinLookupRequest struct {
	Amount                           *Amount `json:"amount",omitempty`
	Assumptions                      string  `json:"assumptions",omitempty`
	CardNumber                       string  `json:"cardNumber",omitempty`
	EncryptedCardNumber              string  `json:"encryptedCardNumber",omitempty`
	MerchantAccount                  string  `json:"merchantAccount",omitempty`
	selectedRecurringDetailReference string  `json:"merchantAccount",omitempty`
}

// Assumptions made for the expected characteristics of the transaction,
// for which the charges are being estimated.
type Assumptions struct {
	Assume3DSecureAuthenticated bool `json:"assume3DSecureAuthenticated"`
	AssumeLevel3Data            bool `json:"assumeLevel3Data"`
	Installments                bool `json:"installments"`
}

// BinLookupResponse is returned by Adyen in response to
// a BinLookup request.
type BinLookupResponse struct {
	CardBin            CardBin            `json:"cardBin"`
	CostEstimateAmount CostEstimateAmount `json:"costEstimateAmount",omitempty`
	ResultCode         string             `json:"resultCode"`
	SurchargeType      string             `json:"surchargeType"`
}

// CardBin is returned by Adyen in response to
// a BinLookup request.
type CardBin struct {
	Bin               string `json:"bin"`
	Commercial        string `json:"commercial"`
	FundingSource     string `json:"fundingSource"`
	FundsAvailability string `json:"fundsAvailability"`
	IssuingBank       string `json:"issuingBank"`
	IssuingCountry    string `json:"IssuingCountry"`
	IssuingCurrency   string `json:"issuingCurrency"`
	PaymentMethod     string `json:"paymentMethod"`
	PayoutEligible    string `json:"payoutEligible"`
	Summary           string `json:"summary"`
}

// CostEstimateAmount is returned by Adyen in response to
// a BinLookup request.
type CostEstimateAmount struct {
	currency string `json:"summary"`
	value    int32  `json:"summary"`
}
