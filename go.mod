module bitbucket.org/makana14/adyen-go-api

go 1.14

require (
	github.com/google/go-querystring v1.0.0
	github.com/joho/godotenv v1.3.0
)
